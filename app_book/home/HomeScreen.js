import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, ImageBackground, BackHandler, Alert } from 'react-native';
import GridView from 'react-native-gridview';

import { Actions } from 'react-native-router-flux';

listContents = [
    { id: 'english_alphabet', title: 'English Alphabets', imageUri: require('./imgs/eng_alphabet.png') },
    { id: 'nep_vowel', title: 'नेपाली वर्णमाला: स्वर वर्ण', imageUri: require('./imgs/nep_vowel.png') },
    { id: 'nep_consonant', title: 'नेपाली वर्णमाला: व्यञ्जन वर्ण', imageUri: require('./imgs/nep_consonant.png') },
    { id: 'number', title: 'Numerical Digit/अङ्क', imageUri: require('./imgs/number.png') },
    { id: 'fruit', title: 'Fruits', imageUri: require('./imgs/fruit.png') },
    { id: 'vegetable', title: 'Vegetables', imageUri: require('./imgs/vegetable.png') },
    { id: 'domestic_animal', title: 'Domestic Animals', imageUri: require('./imgs/domestic_animal.png') },
    { id: 'wild_animal', title: 'Wild Animals', imageUri: require('./imgs/wild_animal.png') },
    { id: 'flower', title: 'Flowers', imageUri: require('./imgs/wild_animal.png') },
    { id: 'bird', title: 'Brids', imageUri: require('./imgs/wild_animal.png') },
    { id: 'water_animal', title: 'Water Animals', imageUri: require('./imgs/wild_animal.png') },
    { id: 'insect', title: 'Insects', imageUri: require('./imgs/wild_animal.png') },
    { id: 'amp_rep', title: 'Amphibians and Reptiles', imageUri: require('./imgs/wild_animal.png') },
    { id: 'computer', title: 'Computers and Camera', imageUri: require('./imgs/wild_animal.png') },
    { id: 'kitchen', title: 'Kitchen Utensils', imageUri: require('./imgs/wild_animal.png') },
    { id: 'luminaries', title: 'National Lumimaries', imageUri: require('./imgs/wild_animal.png') },
    { id: 'transportation', title: 'Means of Transportation', imageUri: require('./imgs/wild_animal.png') },
    { id: 'job', title: 'Jobs and Occupations', imageUri: require('./imgs/wild_animal.png') },
    { id: 'habit', title: 'Good Habits', imageUri: require('./imgs/wild_animal.png') },
    { id: 'game', title: 'Games', imageUri: require('./imgs/wild_animal.png') },
    { id: 'our_action', title: 'Our Actions', imageUri: require('./imgs/wild_animal.png') },
    { id: 'stationery', title: 'Stationery Items', imageUri: require('./imgs/wild_animal.png') },
]

class HomeScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <GridView data={listContents}
                    itemsPerRow={1}
                    itemsPerRowPotrait={2}
                    itemsPerRowLandscape={3}
                    renderItem={(gridItem) => {
                        return (
                            <GridItem gridItem={gridItem} onPress={() => this.openScreen(id)} />
                        )
                    }}
                />
            </View>
        )
    }
}

export default HomeScreen;

class GridItem extends Component {
    render({ gridItem } = this.props) {
        const { id, title, imageUri } = gridItem;
        return (
            <TouchableOpacity style={styles.container} activeOpacity={0.7} onPress={() => this.openScreen(id)}>
                <ImageBackground style={styles.image} source={imageUri}>
                    <Text style={styles.text}>{title}</Text>
                </ImageBackground>

            </TouchableOpacity>
        )
    }

    openScreen = (id) => {
        switch (id) {
            case 'english_alphabet': Actions.english_alphabet(); break;
            case 'nep_vowel': Actions.nep_vowel(); break;
            case 'nep_consonant': Actions.nep_consonant(); break;
            case 'number': Actions.number(); break;
            case 'fruit': Actions.fruit(); break;
            case 'vegetable': Actions.vegetable(); break;
            case 'domestic_animal': Actions.domestic_animal(); break;
            case 'wild_animal': Actions.wild_animal(); break;
            case 'flower': Actions.flower(); break;
            case 'bird': Actions.bird(); break;
            case 'water_animal': Actions.water_animal(); break;
            case 'insect': Actions.insect(); break;
            case 'amp_rep': Actions.amp_rep(); break;
            case 'computer': Actions.computer(); break;
            case 'kitchen': Actions.kitchen(); break;
            case 'luminaries': Actions.luminaries(); break;
            case 'transportation': Actions.transportation(); break;
            case 'job': Actions.job(); break;
            case 'habit': Actions.habit(); break;
            case 'game': Actions.game(); break;
            case 'our_action': Actions.our_action(); break;
            case 'stationery': Actions.stationery(); break;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        elevation: 1,
        borderColor: '#aaa',
        borderBottomWidth: 1,

    },
    text: {
        fontSize: 24,
        color: '#fff',
        textShadowColor: '#222',
        textShadowRadius: 10,
        fontWeight: 'normal',
        textShadowOffset: { width: 1, height: 1 },
        justifyContent: 'center',
        textAlign: 'center'
    },
    image: {
        height: 75,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
})