import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';

class MultipeImageView extends Component {

    render() {

        count = this.props.count;
        images = [];

        for (n = 0; n < count; n++) {
            images.push({ 'key': 'key_' + n, 'uri': this.props.imageUri });
        }

        if (count < 5)
            renderedImages = images.map(n => {
                return <Image style={[styles.image, { height: 140, width: '50%'}]} key={n.key} source={n.uri} />;
            })
        else if (count < 10)
            renderedImages = images.map(n => {
                return <Image style={[styles.image, { height: 90, width: '33%' }]} key={n.key} source={n.uri} />;
            })
        else
            renderedImages = images.map(n => {
                return <Image style={[styles.image, { height: 80, width: '25%' }]} key={n.key} source={n.uri} />;
            })

        return (
            <View style={styles.container}>

                <View style={styles.imagesContainer}>{renderedImages}</View>

            </View>
        )
    }
}

export default MultipeImageView;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        elevation: 6,
    },
    imagesContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        margin: 8
    },
    image: {
        resizeMode: 'contain'
    }
})
