import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Button, Image, ImageBackground } from 'react-native';
import { SketchCanvas } from '@terrylinla/react-native-sketch-canvas';

class NepaliAlphabetView extends Component {

    symbols = this.props.symbols;
    words = this.props.words;
    images = this.props.images;
    drawTemplate = this.props.drawTemplate;

    constructor(props) {
        super(props);
        i = 0;
    }

    loadPrevious = () => {
        if (--i < 0) i = 0;
        this.setState({ i })
        this.clearCanvas();
    }

    loadNext = () => {
        if (++i >= this.symbols.length) i = this.symbols.length - 1;
        this.setState({ i })
        this.clearCanvas();
    }

    clearCanvas = () => {
        this._sketchCanvas.clear();
    }

    render() {
        return (
            <View style={this.styles.container}>
                <HeaderBlock style={this.styles.header}
                    symbol={this.symbols[i]}
                    image={this.images[i]}
                    word={this.words[i]} />

                <ImageBackground style={this.styles.horizontalContainer} imageStyle={{ resizeMode: 'contain' }}
                    source={this.drawTemplate[i]}>
                    <SketchCanvas
                        ref={ref => this._sketchCanvas = ref}
                        style={this.styles.canvas}
                        strokeColor={'red'}
                        strokeWidth={5} />
                </ImageBackground>

                <View style={this.styles.buttonsContainer}>
                    <TouchableOpacity style={this.styles.buttonLeft} onPress={this.loadPrevious} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/button_left.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={this.styles.buttonClear} onPress={this.clearCanvas} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/eraser.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={this.styles.buttonRight} onPress={this.loadNext} activeOpacity={0.8}>
                        <Image style={this.styles.buttonImage} source={require('./../assets/button_right.png')} />
                    </TouchableOpacity>
                </View>

            </View>
        )
    }

    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#fff'
        },
        header: {
        },
        horizontalContainer: {
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
        },
        canvas: {
            flex: 1,
            height: '100%',
            flexGrow: 1,
            backgroundColor: 'transparent'
        },
        buttonsContainer: {
            flexDirection: 'row',
            height: 48,
            width: '100%',
            alignItems: 'stretch',
        },
        buttonLeft: {
            flex:1.5,
            marginRight: 2,
            backgroundColor: '#669FD1',
            borderTopRightRadius: 10
        },
        buttonRight: {
            flex: 1.5,
            marginLeft: 2,
            backgroundColor: '#669FD1',
            borderTopLeftRadius: 10
        },
        buttonClear: {
            flex: 1,
            width: 75,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            backgroundColor: '#669FD1'
        },
        buttonImage: {
            flex: 1,
            margin: 10,
            resizeMode: 'contain',
            alignSelf: 'center'
        }
    })
}

export default NepaliAlphabetView;

class HeaderBlock extends Component {

    render() {
        return (
            <View style={this.styles.container}>
                <TouchableOpacity style={this.styles.characterConainer} activeOpacity={0.9}>
                    <Text style={this.styles.textSymbol}>{this.props.symbol}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={this.styles.imageContainer} activeOpacity={0.9}>
                    <Image style={this.styles.image} source={this.props.image} />

                    <Text style={this.styles.word}>{this.props.word}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    styles = StyleSheet.create({
        container: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: '#fff',
            padding: 12,
            elevation: 5
        },
        characterConainer: {
            height: 150,
            width: 150,
            elevation: 2,
            backgroundColor: '#c0392b',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            elevation: 5
        },
        textSymbol: {
            fontSize: 100,
            color: '#fff'
        },
        imageContainer: {
            height: 160,
            flexGrow: 1,
            backgroundColor: '#fff',
            justifyContent: 'center',
            alignItems: 'center'
        },
        image: {
            width: '90%',
            height: '80%',
            resizeMode: 'contain'
        },
        word: {
            fontSize: 24,
        }
    })
}