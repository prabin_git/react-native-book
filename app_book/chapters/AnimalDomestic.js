import React, { Component } from "react";
import { StyleSheet, Text } from 'react-native';
import GridDisplay from './../components/GridDisplay';

const data = [
    { id: 'dog', title: 'Dog (कुकुर)', imageUri: require('./imgs/animals/domestic/dog.png') },
    { id: 'cat', title: 'Cat (बिरालो)', imageUri: require('./imgs/animals/domestic/cat.png') },
    { id: 'camel', title: 'Camel', imageUri: require('./imgs/animals/domestic/camel.jpg') },
    { id: 'cow', title: 'Cow (गार्इ)', imageUri: require('./imgs/animals/domestic/cow.png') },
    { id: 'buffalo', title: 'Buffalo (भैँसी)', imageUri: require('./imgs/animals/domestic/buffalo.png') },
    { id: 'pig', title: 'Pig (सुँगुर)', imageUri: require('./imgs/animals/domestic/pig.png') },
    { id: 'goat', title: 'Goat (बाख्रा)', imageUri: require('./imgs/animals/domestic/goat.png') },
    { id: 'donkey', title: 'Donkey (गधा)', imageUri: require('./imgs/animals/domestic/donkey.png') },
    { id: 'sheep', title: 'Sheep (भेडा)', imageUri: require('./imgs/animals/domestic/sheep.png') },
    { id: 'yak', title: 'Yak', imageUri: require('./imgs/animals/domestic/yak.jpg') },
]

class AnimalDomestic extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default AnimalDomestic;