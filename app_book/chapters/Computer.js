import React, { Component } from "react";
import GridDisplay from './../components/GridDisplay';

const data = [
    { id: '1', title: 'Camera', imageUri: require('./imgs/computer/Camera.jpg') },
    { id: '2', title: 'CD Rom', imageUri: require('./imgs/computer/CDRom.jpg') },
    { id: '3', title: 'CD', imageUri: require('./imgs/computer/CD.jpg') },
    { id: '4', title: 'Laptop', imageUri: require('./imgs/computer/Laptop.jpg') },
    { id: '5', title: 'Scanner', imageUri: require('./imgs/computer/scanner.jpg') },
    { id: '6', title: 'USB Drive', imageUri: require('./imgs/computer/USBDrive.jpg') },
    { id: '7', title: 'Computer', imageUri: require('./imgs/computer/computer.jpg') },
    { id: '8', title: 'Printer', imageUri: require('./imgs/computer/printer.jpg') },
    { id: '9', title: 'Cell Phone', imageUri: require('./imgs/computer/CellPhone.jpg') },
    // { id: '10', title: '', imageUri: require('./imgs/computer/.jpg') }
]

class Computer extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Computer;