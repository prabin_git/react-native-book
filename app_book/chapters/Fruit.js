import React, { Component } from "react";
import { StyleSheet, Text } from 'react-native';
import GridDisplay from './../components/GridDisplay';

const data = [
    { id: 'apple', title: 'Apple (स्याउ)', imageUri: require('./imgs/fruits/apple.png') },
    { id: 'banana', title: 'Banana (केरा)', imageUri: require('./imgs/fruits/banana.png') },
    { id: 'orange', title: 'Orange (सुन्तला)', imageUri: require('./imgs/fruits/orange.png') },
    { id: 'mango', title: 'Mango (अाँप)', imageUri: require('./imgs/fruits/mango.png') },
    { id: 'papaya', title: 'Papaya (मेवा)', imageUri: require('./imgs/fruits/papaya.png') },
    { id: 'watermelon', title: 'Watermelon (खरबुजा)', imageUri: require('./imgs/fruits/watermelon.png') },
    { id: 'strawberry', title: 'Strawberry (स्ट्रबेरी)', imageUri: require('./imgs/fruits/strawberry.png') },
    { id: 'lychee', title: 'Lychee (लिची)', imageUri: require('./imgs/fruits/lychee.png') },
    { id: 'grapes', title: 'Grapes (अंगुर)', imageUri: require('./imgs/fruits/grapes.png') },
    { id: 'lemon', title: 'Lemon (कागती)', imageUri: require('./imgs/fruits/lemon.png') }
]

class Fruit extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Fruit;