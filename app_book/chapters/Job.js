import React, { Component } from "react";
import FullWidthGridDisplay from '../components/GridDisplay';

const data = [
    { id: '1', title: 'Barber', imageUri: require('./imgs/job/barber.jpg') },
    { id: '2', title: 'Carpenter', imageUri: require('./imgs/job/carpenter.jpg') },
    { id: '3', title: 'Chef', imageUri: require('./imgs/job/chef.jpg') },
    { id: '4', title: 'Doctor', imageUri: require('./imgs/job/doctor.jpg') },
    { id: '5', title: 'Driver', imageUri: require('./imgs/job/driver.jpg') },
    { id: '6', title: 'Farmer', imageUri: require('./imgs/job/farmer.jpg') },
    { id: '7', title: 'Mason', imageUri: require('./imgs/job/mason.jpg') },
    { id: '8', title: 'Nurse', imageUri: require('./imgs/job/nurse.jpg') },
    { id: '9', title: 'Pilot', imageUri: require('./imgs/job/pilot.jpg') },
    { id: '10', title: 'Police', imageUri: require('./imgs/job/police.jpg') },
    { id: '11', title: 'Porter', imageUri: require('./imgs/job/porter.jpg') },
    { id: '12', title: 'Postman', imageUri: require('./imgs/job/postman.jpg') },
    { id: '13', title: 'Potter', imageUri: require('./imgs/job/potter.jpg') },
    { id: '14', title: 'Soldier', imageUri: require('./imgs/job/soldier.jpg') },
    { id: '15', title: 'Teacher', imageUri: require('./imgs/job/teacher.jpg') },
    { id: '16', title: 'Washerman', imageUri: require('./imgs/job/washerman.jpg') },
]

class Job extends Component {
    render() {
        return (
            <FullWidthGridDisplay data={data} />
        )
    }
}

export default Job;