import React, { Component } from "react";
import GridDisplay from '../components/GridDisplay';

const data = [
    { id: '1', title: 'Amaranth', imageUri: require('./imgs/flower/amaranth.jpg') },
    { id: '2', title: 'Chrysanthemum', imageUri: require('./imgs/flower/chrysanthemum.jpg') },
    { id: '3', title: 'Daffodil', imageUri: require('./imgs/flower/daffodil.jpg') },
    { id: '4', title: 'Dahlia', imageUri: require('./imgs/flower/dahlia.jpg') },
    { id: '5', title: 'Daisy', imageUri: require('./imgs/flower/daisy.jpg') },
    { id: '6', title: 'Jasmine', imageUri: require('./imgs/flower/jasmine.jpg') },
    { id: '7', title: 'Lali Guransh', imageUri: require('./imgs/flower/laliguransh.jpg') },
    { id: '8', title: 'Lily', imageUri: require('./imgs/flower/lily.jpg') },
    { id: '9', title: 'Marigold', imageUri: require('./imgs/flower/marigold.jpg') },
    { id: '9', title: 'Orchid', imageUri: require('./imgs/flower/orchid.jpg') },
    { id: '10', title: 'Pansy', imageUri: require('./imgs/flower/pansy.jpg') },
    { id: '11', title: 'Poinsettias', imageUri: require('./imgs/flower/poinsettias.jpg') },
    { id: '12', title: 'Poppy', imageUri: require('./imgs/flower/poppy.jpg') },
    { id: '13', title: 'Rose', imageUri: require('./imgs/flower/rose.jpg') },
    { id: '14', title: 'Sunflower', imageUri: require('./imgs/flower/sunflower.jpg') },
    { id: '15', title: 'Tulip', imageUri: require('./imgs/flower/tulip.jpg') },
    { id: '16', title: 'Ulex', imageUri: require('./imgs/flower/ulex.jpg') }
]

class Flower extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Flower;