import React, { Component } from "react";
import GridDisplay from '../components/GridDisplay';

const data = [
    { id: '1', title: 'Barn Swallow', imageUri: require('./imgs/bird/barnswallow.jpg') },
    { id: '1', title: 'Cock', imageUri: require('./imgs/bird/cock.jpg') },
    { id: '1', title: 'Crow', imageUri: require('./imgs/bird/crow.jpg') },
    { id: '2', title: 'Cuckoo', imageUri: require('./imgs/bird/cuckoo.jpg') },
    { id: '3', title: 'Danphe', imageUri: require('./imgs/bird/danphe.jpg') },
    { id: '1', title: 'Dove', imageUri: require('./imgs/bird/dove.jpg') },
    { id: '4', title: 'Duck', imageUri: require('./imgs/bird/duck.jpg') },
    { id: '5', title: 'Eagle', imageUri: require('./imgs/bird/eagle.jpg') },
    { id: '6', title: 'Hawk', imageUri: require('./imgs/bird/hawk.jpg') },
    { id: '7', title: 'Hen', imageUri: require('./imgs/bird/hen.jpg') },
    { id: '8', title: 'Heron', imageUri: require('./imgs/bird/heron.jpg') },
    { id: '9', title: 'Hummingbird', imageUri: require('./imgs/bird/hummingbird.jpg') },
    { id: '1', title: 'Kingfisher', imageUri: require('./imgs/bird/kingfisher.jpg') },
    { id: '10', title: 'Kiwi', imageUri: require('./imgs/bird/kiwi.jpg') },
    { id: '10', title: 'Magpie', imageUri: require('./imgs/bird/magpie.jpg') },
    { id: '10', title: 'Maina', imageUri: require('./imgs/bird/maina.jpg') },
    { id: '10', title: 'Ostrich', imageUri: require('./imgs/bird/ostrich.jpg') },
    { id: '10', title: 'Owl', imageUri: require('./imgs/bird/owl.jpg') },
    { id: '10', title: 'Parrot', imageUri: require('./imgs/bird/parrot.jpg') },
    { id: '10', title: 'Peacock', imageUri: require('./imgs/bird/peacock.jpg') },
    { id: '10', title: 'Pegion', imageUri: require('./imgs/bird/pegion.jpg') },
    { id: '10', title: 'Penguin', imageUri: require('./imgs/bird/penguin.jpg') },
    { id: '10', title: 'Peacock', imageUri: require('./imgs/bird/peacock.jpg') },
    { id: '10', title: 'Quail', imageUri: require('./imgs/bird/quail.jpg') },
    { id: '10', title: 'Robin', imageUri: require('./imgs/bird/robin.jpg') },
    { id: '10', title: 'Sparrow', imageUri: require('./imgs/bird/sparrow.jpg') },
    { id: '10', title: 'Woodpecker', imageUri: require('./imgs/bird/woodpecker.jpg') },
]

class Bird extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Bird;