import React, { Component } from "react";
import GridDisplay from '../components/GridDisplay';

const data = [
    { id: '1', title: 'Ant', imageUri: require('./imgs/insect/ant.jpg') },
    { id: '2', title: 'Bee', imageUri: require('./imgs/insect/bee.jpg') },
    { id: '3', title: 'Beetle', imageUri: require('./imgs/insect/beetle.jpg') },
    { id: '4', title: 'Bug', imageUri: require('./imgs/insect/bug.jpg') },
    { id: '5', title: 'Centipede', imageUri: require('./imgs/insect/centipede.jpg') },
    { id: '6', title: 'Cockroach', imageUri: require('./imgs/insect/cockroach.jpg') },
    { id: '7', title: 'Cricket', imageUri: require('./imgs/insect/cricket.jpg') },
    { id: '8', title: 'Flea', imageUri: require('./imgs/insect/flea.jpg') },
    { id: '9', title: 'Fly', imageUri: require('./imgs/insect/fly.jpg') },
    { id: '10', title: 'Grasshopper', imageUri: require('./imgs/insect/grasshopper.jpg') },
    { id: '11', title: 'Lady Bird', imageUri: require('./imgs/insect/ladybird.jpg') },
    { id: '12', title: 'Millipede', imageUri: require('./imgs/insect/millipede.jpg') },
    { id: '13', title: 'Mosquito', imageUri: require('./imgs/insect/mosquito.jpg') },
    { id: '14', title: 'Scorpion', imageUri: require('./imgs/insect/scorpion.jpg') },
    { id: '15', title: 'Spider', imageUri: require('./imgs/insect/spider.jpg') },
    { id: '16', title: 'Ticks', imageUri: require('./imgs/insect/ticks.jpg') },
]

class Insect extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Insect;