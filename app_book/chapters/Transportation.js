import React, { Component } from "react";
import GridDisplay from './../components/GridDisplay';

const data = [
    { id: '1', title: 'Boat', imageUri: require('./imgs/transport/boat.jpg') },
    { id: '2', title: 'Bus', imageUri: require('./imgs/transport/bus.jpg') },
    { id: '3', title: 'Car', imageUri: require('./imgs/transport/car.jpg') },
    { id: '4', title: 'Cycle', imageUri: require('./imgs/transport/cycle.jpg') },
    { id: '5', title: 'Helicopter', imageUri: require('./imgs/transport/helicopter.jpg') },
    { id: '6', title: 'Jeep', imageUri: require('./imgs/transport/jeep.jpg') },
    { id: '7', title: 'Motorcycle', imageUri: require('./imgs/transport/motorcycle.jpg') },
    { id: '8', title: 'Plane', imageUri: require('./imgs/transport/plane.jpg') },
    { id: '9', title: 'Rickshaw', imageUri: require('./imgs/transport/rickshaw.jpg') },
    { id: '10', title: 'Ship', imageUri: require('./imgs/transport/ship.jpg') },
    { id: '11', title: 'Taxi', imageUri: require('./imgs/transport/taxi.jpg') },
    { id: '12', title: 'Tempo', imageUri: require('./imgs/transport/tempo.jpg') },
    { id: '13', title: 'Tractor', imageUri: require('./imgs/transport/tractor.jpg') },
    { id: '14', title: 'Train', imageUri: require('./imgs/transport/train.jpg') },
    { id: '15', title: 'Truck', imageUri: require('./imgs/transport/truck.jpg') },
    { id: '16', title: 'Van', imageUri: require('./imgs/transport/van.jpg') },
]

class Transportation extends Component {
    render() {
        return (
            <GridDisplay data={data} />
        )
    }
}

export default Transportation;