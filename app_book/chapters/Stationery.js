import React, { Component } from "react";
import GridDisplay from './../components/GridDisplay';
// import ExerciseScreen from '../exerises/ExerciseScreen';

const data = [
    { id: '1', title: 'Ball Pen', imageUri: require('./imgs/stationery/ballpen.jpg') },
    { id: '2', title: 'Board', imageUri: require('./imgs/stationery/board.jpg') },
    { id: '3', title: 'Book', imageUri: require('./imgs/stationery/book.jpg') },
    { id: '4', title: 'Calculator', imageUri: require('./imgs/stationery/calc.jpg') },
    { id: '5', title: 'Chalk', imageUri: require('./imgs/stationery/chalk.jpg') },
    { id: '6', title: 'Crayon', imageUri: require('./imgs/stationery/crayon.jpg') },
    { id: '7', title: 'Duster', imageUri: require('./imgs/stationery/duster.jpg') },
    { id: '8', title: 'Eraser', imageUri: require('./imgs/stationery/eraser.jpg') },
    { id: '9', title: 'Geometry Box', imageUri: require('./imgs/stationery/geometry_box.jpg') },
    { id: '10', title: 'Glue Stick', imageUri: require('./imgs/stationery/glue_stick.jpg') },
    { id: '11', title: 'Highlighter', imageUri: require('./imgs/stationery/highlighter.jpg') },
    { id: '12', title: 'Ink', imageUri: require('./imgs/stationery/ink.jpg') },
    { id: '13', title: 'Ink Pen', imageUri: require('./imgs/stationery/inkpen.jpg') },
    { id: '14', title: 'Marker', imageUri: require('./imgs/stationery/marker.jpg') },
    { id: '15', title: 'Notebook', imageUri: require('./imgs/stationery/notebook.jpg') },
    { id: '16', title: 'Paper Clip', imageUri: require('./imgs/stationery/paperclip.jpg') },
    { id: '17', title: 'Pencil', imageUri: require('./imgs/stationery/pencil.jpg') },
    { id: '18', title: 'Punching Machine', imageUri: require('./imgs/stationery/punchingmachine.jpg') },
    { id: '19', title: 'Ruler', imageUri: require('./imgs/stationery/ruler.jpg') },
    { id: '20', title: 'School Bag', imageUri: require('./imgs/stationery/school_bag.jpg') },
    { id: '21', title: 'Scissors', imageUri: require('./imgs/stationery/scissors.jpg') },
    { id: '22', title: 'Sharpener', imageUri: require('./imgs/stationery/sharpener.jpg') },
    { id: '23', title: 'Stapler', imageUri: require('./imgs/stationery/stapler.jpg') },
    { id: '24', title: 'Tape Dispenser', imageUri: require('./imgs/stationery/tapedispenser.jpg') },
]

class Stationery extends Component {

    render() {
        const matchPictureData = [
            { id: '1', title: 'Ball Pen', imageUri: require('./imgs/stationery/ballpen.jpg') },
            { id: '2', title: 'Board', imageUri: require('./imgs/stationery/board.jpg') },
            { id: '3', title: 'Book', imageUri: require('./imgs/stationery/book.jpg') },
            { id: '4', title: 'Calculator', imageUri: require('./imgs/stationery/calc.jpg') },
            { id: '5', title: 'Chalk', imageUri: require('./imgs/stationery/chalk.jpg') },
            { id: '6', title: 'Crayon', imageUri: require('./imgs/stationery/crayon.jpg') }
        ];

        return (
            <View>
                <GridDisplay data={data} />
                {/* <ExerciseScreen matchPictureData={matchPictureData} /> */}
            </View>
        )
    }
}

export default Stationery;