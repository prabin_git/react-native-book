// import React, { Component } from 'react';
// import {
//     Image,
//     PanResponder,
//     Text,
//     TouchableOpacity,
//     StyleSheet,
//     ToastAndroid,
//     View,
//     Alert,
// } from 'react-native';
// import Svg, { Line } from 'react-native-svg';

// const lineColor = '#0a7a99';
// const lineError = '#b22222';

// const question_answer = [
//     { Q: 1, A: 1 },
//     { Q: 2, A: 2 },
//     { Q: 3, A: 3 },
//     { Q: 4, A: 4 },
//     { Q: 5, A: 5 },
//     { Q: 6, A: 6 },
// ];

// var lines = [
//     { x1: 0, y1: 0, x2: 0, y2: 0 },
//     { x1: 0, y1: 0, x2: 0, y2: 0 },
//     { x1: 0, y1: 0, x2: 0, y2: 0 },
//     { x1: 0, y1: 0, x2: 0, y2: 0 },
//     { x1: 0, y1: 0, x2: 0, y2: 0 },
//     { x1: 0, y1: 0, x2: 0, y2: 0 }
// ];

// export default class MatchTheFollowing extends Component {

//     state = {
//         x1: 0,
//         y1: 0,
//         x2: 0,
//         y2: 0,
//         xmin: 0,
//         ymin: 0,
//         xmax: 0,
//         ymax: 0,
//         isDrawing: false,
//     }

//     constructor(props) {
//         super(props);

//         this.state = {
//             image_text: this.props.dataset,
//             linesColor: [lineColor, lineColor, lineColor, lineColor, lineColor, lineColor]
//         }
//         this._panResponder = PanResponder.create({
//             // Ask to be the responder:
//             onStartShouldSetPanResponder: (evt, gestureState) => true,
//             onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
//             onMoveShouldSetPanResponder: (evt, gestureState) => true,
//             onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

//             onPanResponderGrant: (evt, gestureState) => {
//                 this.setState({
//                     x1: 0,
//                     y1: 0,
//                     x2: 0,
//                     y2: 0
//                 })
//             },

//             onPanResponderMove: (evt, gestureState) => {
//                 const newX = gestureState.moveX;
//                 const newY = gestureState.moveY;

//                 if (this.state.isDrawing) {    //select second point
//                     this.setState({
//                         x2: newX,
//                         y2: newY
//                     })
//                 } else {    //select first point
//                     this.setState({
//                         x1: newX,
//                         y1: newY,
//                         isDrawing: true
//                     })
//                 }

//                 // console.log(gestureState.moveX);
//                 // console.log(gestureState.moveY);
//             },
//             onPanResponderTerminationRequest: (evt, gestureState) => true,
//             onPanResponderRelease: (evt, gestureState) => {
//                 // console.log('Release X:', gestureState.moveX);
//                 // console.log('Release Y:', gestureState.moveY);

//                 this.setState({
//                     isDrawing: false
//                 })

//                 this.checkTwoPoints();

//             },
//             onPanResponderTerminate: (evt, gestureState) => { },
//             onShouldBlockNativeResponder: (evt, gestureState) => { return true; },
//         });
//     }

//     componentDidMount() {

//         this.shuffleData();
//     }

//     shuffleData() {

//         var data = this.state.image_text;
//         //console.log(data);

//         var textAns = [];                                   //original sequence of text
//         data.map(i => { textAns.push(i.text); })

//         //clone array to new array, direct assignment DOESN'T WORK
//         var array = [];
//         textAns.map(i => {
//             array.push(i);
//         })

//         //shuffle position of answers
//         var shuffledAns = this.shuffleArray(array);

//         for (i = 0; i < 6; i++) {
//             //find index of the real answer in the shuffled answer and store as solution
//             const newIndex = shuffledAns.indexOf(textAns[i]);
//             question_answer[i].A = newIndex + 1;
//             data[i].text = shuffledAns[i];
//         }

//         console.log(question_answer);
//         console.log(data);

//         this.setState({
//             image_text: data
//         })
//     }

//     shuffleArray(a) {
//         var currentIndex = a.length, tempValue, randomIndex;
//         while (0 !== currentIndex) {

//             // Pick a remaining element...
//             randomIndex = Math.floor(Math.random() * currentIndex);
//             currentIndex -= 1;

//             // And swap it with the current element.
//             tempValue = a[currentIndex];
//             a[currentIndex] = a[randomIndex];
//             a[randomIndex] = tempValue;
//         }
//         console.log(a);

//         return a;
//     }

//     checkTwoPoints() {
//         const x1 = this.state.x1;
//         const y1 = this.state.y1;
//         const x2 = this.state.x2;
//         const y2 = this.state.y2;

//         const height = this.state.ymax;
//         const width = this.state.xmax;

//         var block1, block2;

//         block1 = getBlock1(x1, y1, height, width);
//         block2 = getBlock2(x2, y2, height, width);
//         // console.log(block1);
//         // console.log(block2);

//         if (block1 == -1 || block2 == -1) {
//             ToastAndroid.show('Try again', ToastAndroid.show);
//             this.setState({
//                 x1: 0, y1: 0, x2: 0, y2: 0
//             })
//             return;
//         }

//         //assign coordinates to correspoding line
//         lines[block1 - 1].x1 = x1;
//         lines[block1 - 1].y1 = y1;
//         lines[block1 - 1].x2 = x2;
//         lines[block1 - 1].y2 = y2;

//         if (checkAnswer(block1, block2)) {
//             // ToastAndroid.show(block1 + ' ' + block2 + ' match', ToastAndroid.show);
//         } else {
//             // ToastAndroid.show(block1 + ' ' + block2 + ' don\'t match', ToastAndroid.show);
//         }

//         //remove temporary line
//         this.setState({
//             x1: 0, y1: 0, x2: 0, y2: 0
//         })
//     }

//     drawLine() {
//         //temporary line
//         //disable line if first point is at (0,0)
//         if (this.state.x1 == 0 || this.state.x2 == 0) {
//             return <View />
//         } else {
//             return (
//                 <Svg style={{ position: 'absolute' }} height='100%' width='100%' viewBox={'0 0 ' + this.state.xmax + ' ' + this.state.ymax}>
//                     <Line x1={this.state.x1} y1={this.state.y1}
//                         x2={this.state.x2} y2={this.state.y2}
//                         stroke={lineColor} strokeWidth='3' />
//                 </Svg>
//             )
//         }
//     }

//     canvas() {
//         const linesColor = this.state.linesColor;
//         // console.log(linesColor)
//         return (
//             <Svg height='100%' width='100%' viewBox={'0 0 ' + this.state.xmax + ' ' + this.state.ymax}>

//                 <Line x1={lines[0].x1} y1={lines[0].y1}
//                     x2={lines[0].x2} y2={lines[0].y2}
//                     stroke={linesColor[0]} strokeWidth='2' />

//                 <Line x1={lines[1].x1} y1={lines[1].y1}
//                     x2={lines[1].x2} y2={lines[1].y2}
//                     stroke={linesColor[1]} strokeWidth='2' />

//                 <Line x1={lines[2].x1} y1={lines[2].y1}
//                     x2={lines[2].x2} y2={lines[2].y2}
//                     stroke={linesColor[2]} strokeWidth='2' />

//                 <Line x1={lines[3].x1} y1={lines[3].y1}
//                     x2={lines[3].x2} y2={lines[3].y2}
//                     stroke={linesColor[3]} strokeWidth='2' />

//                 <Line x1={lines[4].x1} y1={lines[4].y1}
//                     x2={lines[4].x2} y2={lines[4].y2}
//                     stroke={linesColor[4]} strokeWidth='2' />

//                 <Line x1={lines[5].x1} y1={lines[5].y1}
//                     x2={lines[5].x2} y2={lines[5].y2}
//                     stroke={linesColor[5]} strokeWidth='2' />

//             </Svg>
//         )
//     }

//     checkAllAnswer() {
//         const height = this.state.ymax;
//         const width = this.state.xmax;

//         var linesColor = this.state.linesColor;


//         for (i = 0; i < lines.length; i++) {

//             var block1 = getBlock1(lines[i].x1, lines[i].y1, height, width);
//             var block2 = getBlock2(lines[i].x2, lines[i].y2, height, width);

//             if (block1 == -1 || block2 == -1) {
//                 ToastAndroid.show('Please attempt all before checking', ToastAndroid.SHORT);
//                 return;
//             }
//         }

//         var allCorrectFlag = true;
//         for (i = 0; i < lines.length; i++) {

//             var block1 = getBlock1(lines[i].x1, lines[i].y1, height, width);
//             var block2 = getBlock2(lines[i].x2, lines[i].y2, height, width);

//             if (checkAnswer(block1, block2)) {
//                 linesColor[i] = lineColor;
//             } else {
//                 linesColor[i] = lineError;
//                 allCorrectFlag = false;
//             }
//         }

//         this.setState({ linesColor: linesColor })

//         if(allCorrectFlag) {
//             Alert.alert(
//                 '',
//                 'All answers are correct',
//                 [{
//                     text:'Start Again', onPress: ()=> {this.reset()}
//                 },
//                 {
//                     text: 'Exit', onPress: ()=>{this.exitScreen()}
//                 }
//                 ])
//         }
//     }

//     render() {

//         const renderImageSet = this.state.image_text.map(item => {
//             return (
//                 <Image style={styles.questionImage}
//                     source={item.image} />
//             )
//         })

//         const renderTextSet = this.state.image_text.map(item => {
//             return (
//                 <Text style={styles.answerText}>{item.text}</Text>
//             )
//         })

//         return (
//             <View style={styles.container}>

//                 <Text style={{ fontSize: 14, color: '#aaa' }}>Match the following pictures</Text>

//                 <View style={styles.main}>


//                     <View style={styles.questionSet}>

//                         {renderImageSet}

//                         {/* <Svg height='100%' width='100%' viewBox={'0 0 ' + xmax + ' ' + ymax}>
//             <Rect x='0' y={ymax - (ymax / 6) * 6} height={ymax / 6 - 1} width='85' fill='red' />
//             <Rect x='0' y={ymax - (ymax / 6) * 5} height={ymax / 6 - 1} width='85' fill='green' />
//             <Rect x='0' y={ymax - (ymax / 6) * 4} height={ymax / 6 - 1} width='85' fill='blue' />
//             <Rect x='0' y={ymax - (ymax / 6) * 3} height={ymax / 6 - 1} width='85' fill='cyan' />
//             <Rect x='0' y={ymax - (ymax / 6) * 2} height={ymax / 6 - 1} width='85' fill='black' />
//             <Rect x='0' y={ymax - (ymax / 6) * 1} height={ymax / 6 - 1} width='85' fill='orange' />
            
//         </Svg> */}

//                         <View style={styles.answerSet}>
//                             {renderTextSet}
//                         </View>
//                     </View>

//                     <View style={styles.touchable}
//                         {...this._panResponder.panHandlers}
//                         onLayout={event => {
//                             var { x, y, width, height } = event.nativeEvent.layout;
//                             this.setState({
//                                 xmax: width,
//                                 ymax: height
//                             })
//                         }}>

//                         {this.canvas()}
//                         {this.drawLine()}

//                     </View>

//                 </View>
//                 <TouchableOpacity style={{ height: 40, width: '100%', backgroundColor: lineColor }}
//                     activeOpacity={0.9}
//                     onPress={() => this.checkAllAnswer()}>
//                     <Text style={{ height: '100%', width: '100%', textAlign: 'center', padding: 10, color: lineColor, fontWeight: 'bold', backgroundColor: '#fff' }}>
//                         CHECK ANSWER
//                     </Text>
//                 </TouchableOpacity>
//             </View>
//         );
//     }
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         width: '100%',
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: '#fff',
//     },
//     main: {
//         flex: 1,
//         width: '100%'
//     },
//     touchable: {
//         height: '100%',
//         width: '100%',
//         position: 'absolute',
//     },
//     questionSet: {
//         height: '100%',
//         width: '100%',
//         justifyContent: 'space-between',
//         position: 'absolute',
//         left: 0
//     },
//     answerSet: {
//         position: 'absolute',
//         height: '100%',
//         right: 0,
//         justifyContent: 'space-between',
//     },
//     questionImage: {
//         flex: 1,
//         width: 85,
//         resizeMode: 'contain',
//         margin: 1,
//     },
//     answerText: {
//         flex: 1,
//         width: 100,
//         margin: 1,
//         color: '#111',
//         fontSize: 16,
//         textAlignVertical: 'center',
//         textAlign: 'center',
//         borderRadius: 1,
//     }
// });

// function getBlock1(x, y, height, width) {

//     const lv0 = height - 6 * height / 6;
//     const lv1 = height - 5 * height / 6;
//     const lv2 = height - 4 * height / 6;
//     const lv3 = height - 3 * height / 6;
//     const lv4 = height - 2 * height / 6;
//     const lv5 = height - 1 * height / 6;
//     const lv6 = height - 0 * height / 6;

//     //check if line is drawn outside the block horizontally
//     if (x > 85) {
//         return -1;
//     }

//     //check if line is drawn outside the block vetically
//     if (y > lv0 && y < lv1) {
//         return 1;
//     } else if (y >= lv1 && y < lv2) {
//         return 2;
//     } else if (y >= lv2 && y < lv3) {
//         return 3;
//     } else if (y >= lv3 && y < lv4) {
//         return 4;
//     } else if (y >= lv4 && y < lv5) {
//         return 5;
//     } else if (y >= lv5 && y < lv6) {
//         return 6;
//     } else {
//         return -1;
//     }
// }

// function getBlock2(x, y, height, width) {

//     const lv0 = height - 6 * height / 6;
//     const lv1 = height - 5 * height / 6;
//     const lv2 = height - 4 * height / 6;
//     const lv3 = height - 3 * height / 6;
//     const lv4 = height - 2 * height / 6;
//     const lv5 = height - 1 * height / 6;
//     const lv6 = height - 0 * height / 6;

//     //check if line is drawn outside the block horizontally
//     if (x < width - 100) {
//         return -1;
//     }

//     //check if line is drawn outside the block vetically
//     if (y > lv0 && y < lv1) {
//         return 1;
//     } else if (y >= lv1 && y < lv2) {
//         return 2;
//     } else if (y >= lv2 && y < lv3) {
//         return 3;
//     } else if (y >= lv3 && y < lv4) {
//         return 4;
//     } else if (y >= lv4 && y < lv5) {
//         return 5;
//     } else if (y >= lv5 && y < lv6) {
//         return 6;
//     } else {
//         return -1;
//     }
// }

// function checkAnswer(block1, block2) {
//     // console.log(question_answer);
//     // var i1 = question_answer.indexOf({Q:block1, A: block2});
//     i1 = question_answer.map(QA => { return QA.Q }).indexOf(block1);
//     i2 = question_answer.map(QA => { return QA.A }).indexOf(block2);
//     // console.log(i1, i2);
//     if (i1 == i2) return true;
//     else return false;
// }